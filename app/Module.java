import com.google.inject.AbstractModule;
import models.HelperPools;
import play.libs.akka.AkkaGuiceSupport;

public class Module extends AbstractModule implements AkkaGuiceSupport {

	@Override
	protected void configure() {
		bind(HelperPools.class).asEagerSingleton();
	}
}
