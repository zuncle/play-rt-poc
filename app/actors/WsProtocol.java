package actors;

import lombok.EqualsAndHashCode;
import lombok.Value;
import models.AlarmState;
import models.DataValue;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public interface WsProtocol {

	enum TopicType {
		DATA_VALUE,
		ALARM_STATE,
		_KEEP_ALIVE,
		_CLOSE
	}

	static class Message {
		final long issueDate = LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli();
	}

	@Value
	@EqualsAndHashCode(callSuper=true)
	class DataValueMessage extends Message {
		TopicType topic = TopicType.DATA_VALUE;
		final DataValue payload;
	}

	@Value
	@EqualsAndHashCode(callSuper=true)
	class AlarmStateMessage extends Message {
		TopicType topic = TopicType.ALARM_STATE;
		final AlarmState payload;
	}

	@Value
	@EqualsAndHashCode(callSuper=true)
	class KeepAliveMessage extends Message {
		TopicType topic = TopicType._KEEP_ALIVE;
		final KeepAlivePayload payload;
		final String ipAddress;
		@Value
		static class KeepAlivePayload {
			long duration;
		}
	}

	@Value
	@EqualsAndHashCode(callSuper=true)
	class CloseMessage extends Message {
		TopicType topic = TopicType._CLOSE;
		final String ipAddress;
	}
}
