package actors;

import lombok.Value;

@Value
public class DisableKeepAlive {
	final boolean disabled;
}
