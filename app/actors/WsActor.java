package actors;

import akka.actor.*;
import models.AlarmState;
import models.DataValue;
import play.Logger;
import play.libs.Json;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import static play.Logger.of;

public class WsActor extends AbstractActor {

	private static final Logger.ALogger logger = of(WsActor.class);

	public static Props props(ActorRef out) {
		return Props.create(WsActor.class, out);
	}

	private final FiniteDuration KEEP_ALIVE_DURATION = Duration.create(7, TimeUnit.SECONDS);
	private final ExecutionContext executionContext;
	private final ActorRef out;

	private String poolCode;
	private String hostAdress = "0.0.0.0";
	private Cancellable cancelKeepAliveMessage;

	private boolean disabledKeepAlive = false;

	public WsActor(ActorRef out, ExecutionContext executionContext){
		this.out = out;
		this.executionContext = executionContext;

		try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			this.hostAdress = inetAddress.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void preStart() throws Exception {
		super.preStart();
		logger.trace("preStart poolCode:{}", poolCode);

		context().system().eventStream().subscribe(self(), KillWsActor.class);
		context().system().eventStream().subscribe(self(), DisableKeepAlive.class);
		context().system().eventStream().subscribe(self(), DataValue.class);
		context().system().eventStream().subscribe(self(), AlarmState.class);

		// start keep alive web socket message
		scheduleKeepAliveMessage();
	}

	@Override
	public void postStop() throws Exception {
		close();
		cancelKeepAliveMessage();
		super.postStop();
		logger.debug("postStop");
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(WsProtocol.KeepAliveMessage.class, this::onKeepAlive)
				.match(KillWsActor.class, this::onKillActor)
				.match(DisableKeepAlive.class, this::onDisableKeepAlive)
				.match(DataValue.class, this::onDataValue)
				.match(AlarmState.class, this::onAlarmState)
				.build();
	}

	private void scheduleKeepAliveMessage(){
		if ( !this.disabledKeepAlive ) {
			this.cancelKeepAliveMessage = context().system().scheduler().scheduleWithFixedDelay(
					Duration.create(0, TimeUnit.SECONDS), //KEEP_ALIVE_DURATION,
					KEEP_ALIVE_DURATION,
					this.self(),
					new WsProtocol.KeepAliveMessage(
							new WsProtocol.KeepAliveMessage.KeepAlivePayload(KEEP_ALIVE_DURATION.toMillis()),
							this.hostAdress),
					executionContext,
					this.self());
		}
	}

	private void cancelKeepAliveMessage(){
		if ( this.cancelKeepAliveMessage != null && !this.cancelKeepAliveMessage.isCancelled())
			this.cancelKeepAliveMessage.cancel();
	}

	private void onKeepAlive(WsProtocol.KeepAliveMessage message){
		//if ( !this.disabledKeepAlive )
			out.tell(Json.toJson(message), self());
	}

	private void close(){
		out.tell(Json.toJson(new WsProtocol.CloseMessage(this.hostAdress)), self());
	}

	private void onKillActor(KillWsActor killWsActor){
		context().system().stop(this.self());
	}

	private void onDisableKeepAlive(DisableKeepAlive disableKeepAlive){
		this.disabledKeepAlive = disableKeepAlive.isDisabled();
		if ( this.disabledKeepAlive ){
			this.cancelKeepAliveMessage.cancel();
		}
		else {
			scheduleKeepAliveMessage();
		}
		logger.trace("KeepAlive " + (this.disabledKeepAlive ? "disabled": "enabled"));
	}

	private void onDataValue(DataValue dataValue){
		// TODO to be filtered by pool  code
		out.tell(Json.toJson(new WsProtocol.DataValueMessage(dataValue)), self());
	}

	private void onAlarmState(AlarmState alarmState){
		// TODO to be filtered by pool code
		out.tell(Json.toJson(new WsProtocol.AlarmStateMessage(alarmState)), self());
	}

}
