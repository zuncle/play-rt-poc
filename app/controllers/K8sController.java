package controllers;

import akka.actor.ActorSystem;
import models.DataValue;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;

import static play.Logger.of;

public class K8sController extends Controller {

	private static final Logger.ALogger logger = of(K8sController.class);

	public Result ready(){
		return ok();
	}

	public Result healthy(){
		return ok();
	}
}
