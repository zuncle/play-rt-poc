package controllers;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SubmitDataValueRequest {
	@NotNull
	private String timeSeriesCode;

	private Double value;
}
