package controllers;

import actors.DisableKeepAlive;
import actors.KillWsActor;
import actors.WsActor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.Materializer;
import play.Logger;
import play.libs.streams.ActorFlow;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import scala.concurrent.ExecutionContext;

import javax.inject.Inject;

import static play.Logger.of;

public class WsController extends Controller {

	private static final Logger.ALogger logger = of(WsController.class);

	@Inject
	private ExecutionContext executionContext;

	@Inject
	Materializer materializer;

	@Inject
	ActorSystem actorSystem;

	public WebSocket ws(String poolCode) {
		if ( poolCode == null || poolCode.isEmpty()) {
			//poolCode = null;
			logger.warn("WebSocket has no specified poolCode");
		}
		else
			logger.trace("WebSocket created for poolCode:{}", poolCode);

		return WebSocket.Json.accept(
				request -> ActorFlow.actorRef(
						out -> Props.create(WsActor.class, out, executionContext), actorSystem, materializer));
	}

	public Result killWsActors(){
		actorSystem.eventStream().publish(new KillWsActor());
		return ok();
	}

	public Result enableKeepAlive(){
		actorSystem.eventStream().publish(new DisableKeepAlive(false));
		return ok();
	}

	public Result disableKeepAlive(){
		actorSystem.eventStream().publish(new DisableKeepAlive(true));
		return ok();
	}
}
