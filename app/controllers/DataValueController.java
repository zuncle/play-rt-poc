package controllers;

import actors.DisableKeepAlive;
import actors.KillWsActor;
import actors.WsActor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.Materializer;
import models.DataValue;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.streams.ActorFlow;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.WebSocket;
import scala.concurrent.ExecutionContext;

import javax.inject.Inject;

import static play.Logger.of;

public class DataValueController extends Controller {

	private static final Logger.ALogger logger = of(DataValueController.class);

	@Inject
	FormFactory formFactory;

	@Inject
	ActorSystem actorSystem;

	public Result submit(Http.Request request) {
		Form<SubmitDataValueRequest> form = this.formFactory.form(SubmitDataValueRequest.class).bindFromRequest(request);
		if (form.hasErrors()) {
			return badRequest(form.errorsAsJson());
		}
		SubmitDataValueRequest submitDataValueRequest = form.get();

		DataValue dataValue = new DataValue(submitDataValueRequest.getTimeSeriesCode(), submitDataValueRequest.getValue());
		actorSystem.eventStream().publish(dataValue);

		return ok(Json.toJson(dataValue));
	}
}
