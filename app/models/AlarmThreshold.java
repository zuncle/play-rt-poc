package models;

import lombok.Value;

@Value
public class AlarmThreshold {
	String timeSeriesCode;
	String alarmListCode;
}
