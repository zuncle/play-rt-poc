package models;

import lombok.Value;

@Value
public class AlarmState {
	String timesSeriesCode;
	String alarmListCode;
	String color;
}
