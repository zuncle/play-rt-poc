package models;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class Pool {

	String name;
	List<TimeSeries> timeSeriesList = new ArrayList<>();
	List<AlarmThreshold> alarmThresholdList = new ArrayList<>();


	public Pool addTimeSeries(TimeSeries timeSeries){
		this.timeSeriesList.add(timeSeries);
		return this;
	}

	public Pool addAlarmThreshold(AlarmThreshold alarmThreshold){
		this.alarmThresholdList.add(alarmThreshold);
		return this;
	}
}
