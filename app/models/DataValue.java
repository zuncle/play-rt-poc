package models;

import lombok.Value;

@Value
public class DataValue {
	String timeSeriesCode;
	double value;
}
