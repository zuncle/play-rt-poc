package models;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class Pools {
	List<Pool> poolList = new ArrayList<>();

	public Pools add(Pool pool){
		this.poolList.add(pool);
		return this;
	}
}
