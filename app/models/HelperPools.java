package models;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class HelperPools {

	Pools pools = new Pools();

	@Inject
	HelperPools(){
		pools
			.add(new Pool("POOL1")
				.addTimeSeries(new TimeSeries("AAA"))
				.addTimeSeries(new TimeSeries("BBB"))
				.addTimeSeries(new TimeSeries("CCC"))
				.addAlarmThreshold(new AlarmThreshold("AAA", "ALM1"))
				.addAlarmThreshold(new AlarmThreshold("BBB", "ALM1"))
				.addAlarmThreshold(new AlarmThreshold("CCC", "ALM2"))
		)
		.add(new Pool("POOL2")
				.addTimeSeries(new TimeSeries("DDD"))
				.addTimeSeries(new TimeSeries("EEE"))
				.addAlarmThreshold(new AlarmThreshold("DDD", "ALM2"))
				.addAlarmThreshold(new AlarmThreshold("EEE", "ALM3"))
				.addAlarmThreshold(new AlarmThreshold("FFF", "ALM3"))
		);
	}

}
