// k6 run --vus 10 ws.js
import ws from "k6/ws";
import { check } from "k6";

export default function() {
    const url = "ws://localhost:9000/ws";
    const params = { tags: { poolCode: "hello" } };

    const res = ws.connect(url, params, function(socket) {
        socket.on("open", () => console.log("connected"));
        socket.on("message", data => console.log("Message received: ", data));
        socket.on("close", () => console.log("disconnected"));

        socket.on("ping", () => console.log("PING!"));
        socket.on("pong", () => console.log("PONG!"));

        socket.setTimeout(function() {
            console.log("2 seconds passed, closing the socket");
            socket.close();
        }, 20000);

        socket.setInterval(function timeout() {
            socket.ping();
            console.log("Pinging every 1sec (setInterval test)");
        }, 1000);
    });

    check(res, { "status is 101": r => r && r.status === 101 });
}