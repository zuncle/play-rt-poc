// k6 run --vus 10 enableKeepAlive.js
import http from 'k6/http';
import { check } from "k6";

export default function() {
    const url = "http://localhost:9000/keepAlive";

    let res = http.put(url);
    check(res, {
        'delete succeeded': res => res.status === 200,
    }) || fail('delete failed');
}