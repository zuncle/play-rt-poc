// $ k6 run simple.js
import http from 'k6/http';
import { sleep } from 'k6';

export default function() {
    http.get('http://localhost:9000/alive');
    sleep(1);
}