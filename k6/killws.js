// k6 run --vus 10 killws.js
import http from 'k6/http';
import { check } from "k6";

export default function() {
    const url = "http://localhost:9000/ws";

    let res = http.del(url);
    check(res, {
        'delete succeeded': res => res.status === 200,
    }) || fail('delete failed');
}