// $ k6 run dataValue.js
import http from 'k6/http';
import { check, fail } from 'k6';

const baseURL = 'http://localhost:9000';

export default function() {

    let formdata = {
        timeSeriesCode: 'AAA',
        value: 12.7,
    };

    let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

    let res = http.post(baseURL + '/dataValue', formdata, { headers: headers });
    check(res, {
        'post data value succeeded': res => res.type === `DATA_VALUE`,
    }) || fail('post data value failed');
}