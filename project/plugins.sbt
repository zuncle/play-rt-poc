// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.0")

addSbtPlugin("com.dwijnand" % "sbt-dynver" % "4.0.0")
