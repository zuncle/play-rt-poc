import React from 'react';
import PropTypes from 'prop-types';

export default class AliveWebSocket extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            ws: this.createWebSocket(this.props.url),
            keepAliveTimeOutId : null,
            reconnectionTimeOutId : null
        };
    }

    componentDidMount() {
        this.setupWebsocket();
    }

    componentWillUnmount() {
        this.closeWebSocket();
    }

    createWebSocket(url){
        return window.WebSocket
            ? new window.WebSocket(url)
            : new window.MozWebSocket(url)
    }

    openWebSocket = () => {
        let websocket = this.state.ws
        if ( websocket != null ) return
        this.setState( {
            ws: this.createWebSocket(this.props.url)
        });
    }

    closeWebSocket = () => {
        this.abortKeepAliveTimeOut()
        let websocket = this.state.ws
        if ( websocket != null ) {
            websocket.close();
            this.setState({ws: null})
        }
    }

    reconnect = () => {
        let timeOutId = setTimeout(() => {
            this.openWebSocket()
            this.setupWebsocket()
            this.setState({reconnectionTimeOutId: null})
        }, this.props.reconnectionTimeOut);
        this.setState({reconnectionTimeOutId: timeOutId})
    }

    abortReconnectionTimeOut = () => {
        if ( this.state.reconnectionTimeOutId != null ){
            this.log("current reconnectionTimeOutId aborted")
            clearTimeout(this.state.reconnectionTimeOutId);
        }
    }

    startKeepAliveTimeOut = (duration) => {
        this.log("keepAliveTimeOut started -> " + duration*1.5 + "ms")
        let timeOutId = setTimeout(() => {
            this.log('KeepAliveTimeOut reached');

            this.closeWebSocket()
            this.reconnect()

        }, duration*1.5);
        this.setState({keepAliveTimeOutId: timeOutId})
    }

    abortKeepAliveTimeOut = () => {
        if ( this.state.keepAliveTimeOutId != null ){
            this.log("current keepAliveTimeout aborted")
            clearTimeout(this.state.keepAliveTimeOutId);
            this.setState({keepAliveTimeOutId: null})
        }
    }

    setupWebsocket = () => {
        let websocket = this.state.ws;
        if ( websocket == null ) return;

        websocket.onopen = () => {
            this.log('Websocket connected');
            if (typeof this.props.onOpen === 'function') this.props.onOpen();
        };

        websocket.onclose = () => {
            this.log('Websocket closed');
            if (typeof this.props.onClose === 'function') this.props.onClose();
            this.log("Wait " + this.props.reconnectionTimeOut + " ms to reconnect...")
            this.closeWebSocket()
            this.reconnect()
        };

        websocket.onerror = e => {
            if (typeof this.props.onError === 'function') this.props.onError(e);
            this.closeWebSocket()
            this.reconnect();
        };

        websocket.onmessage = evt => {
            this.handleMessage(evt.data)
        };
    }

    handleMessage(data) {
        let message = JSON.parse(data);
        if ( message.topic === "_KEEP_ALIVE" ){
            this.log(data)
            this.abortKeepAliveTimeOut();
            this.startKeepAliveTimeOut(message.payload.duration)
        }
        else if ( message.topic === "_CLOSE" ){
            this.log(data)
        }
        else
        {
            this.props.onMessage(message);
        }
    }

    render() {
        return null;
    }

    log(message) {
        if ( this.props.debug ){
            console.log(message)
        }
    }
}

AliveWebSocket.defaultProps = {
    reconnectionTimeOut : 3000,
    onMessage : (msg) => console.log(msg),
    debug: false
};

AliveWebSocket.propTypes = {
    url: PropTypes.string.isRequired,
    onMessage: PropTypes.func.isRequired,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    onError: PropTypes.func,
    reconnectionTimeOut: PropTypes.number,
    debug: PropTypes.bool
};
