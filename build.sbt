import com.typesafe.sbt.packager.docker.DockerChmodType

name := """play-rt-poc"""
organization := "fr.pixime"

scalaVersion := "2.13.1"

//version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(PlayJava)
  .settings(dockerSettings)
  .settings(
    libraryDependencies ++= Seq(
      guice,
      lombok
    )
  )

val lombok = "org.projectlombok" % "lombok" % "1.18.8"

def dockerSettings = Seq(
  dockerRepository := Some("zuncle"),
  dockerUpdateLatest := true,
  dockerExposedPorts := Seq(9000),
  dockerChmodType := DockerChmodType.UserGroupWriteExecute,
  dockerExposedVolumes := Seq("/opt/docker/logs", "/opt/docker/conf")
)